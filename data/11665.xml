<?xml version="1.0" encoding="utf-8"?><!DOCTYPE bugzilla  SYSTEM 'https://bugs.mageia.org/page.cgi?id=bugzilla.dtd'><bugzilla maintainer="sysadmin@group.mageia.org" urlbase="https://bugs.mageia.org/" version="4.4.11">

    <bug>
          <bug_id>11665</bug_id>
          
          <creation_ts>2013-11-13 21:06:00 +0100</creation_ts>
          <short_desc>Please remove CAcert.org certificate from rootcerts</short_desc>
          <delta_ts>2015-12-21 16:21:18 +0100</delta_ts>
          <reporter_accessible>1</reporter_accessible>
          <cclist_accessible>1</cclist_accessible>
          <classification_id>1</classification_id>
          <classification>Unclassified</classification>
          <product>Mageia</product>
          <component>RPM Packages</component>
          <version>Cauldron</version>
          <rep_platform>All</rep_platform>
          <op_sys>Linux</op_sys>
          <bug_status>NEW</bug_status>
          <resolution/>
          
          
          <bug_file_loc>http://bugs.debian.org/718434</bug_file_loc>
          <status_whiteboard/>
          <keywords/>
          <priority>Normal</priority>
          <bug_severity>normal</bug_severity>
          <target_milestone>---</target_milestone>
          <dependson>11398</dependson>
          
          <everconfirmed>1</everconfirmed>
          <reporter name="Geoffrey Thomas">mageia</reporter>
          <assigned_to name="pkg-bugs">pkg-bugs</assigned_to>
          <cc>luigiwalser</cc>
    
    <cc>marja11</cc>
    
    <cc>oe</cc>
    
    <cc>thomas</cc>
          
          <cf_rpmpkg>rootcerts-20130411.00-2.mga4.src.rpm</cf_rpmpkg>
          <cf_cve/>

      

      

      

          <comment_sort_order>oldest_to_newest</comment_sort_order>  
          <long_desc isprivate="0">
    <commentid>90527</commentid>
    <comment_count>0</comment_count>
    <who name="Geoffrey Thomas">mageia</who>
    <bug_when>2013-11-13 21:06:45 +0100</bug_when>
    <thetext>Debian is considering removing CAcert.org from its root certificate package for a couple of reasons:
- It has not passed the standard Webtrust audit needed for inclusion in the major vendors' CA bundles (Mozilla, Google, Apple, MS, ...)
- It has a history of serious security issues that seem to be systemic to the implementation, and there doesn't seem to be a serious emphasis on code security.
- There are allegedly licensing issues associated with redistributing the root.
- It does not seem to be compliant with current CA/Browser Forum best practices for security. (CAcert.org is not a member of the CA/Browser Forum.)
I think it makes sense for Mageia to drop CAcert.org's root certificate, too.

Take a look at http://bugs.debian.org/718434 for the discussion. In particular, please note Ansgar Burchardt's email from September 16 identifying a shell injection vulnerability in CAcert's signing code (allowing, among other things, arbitrary certificates to be signed), and please note the general quality of that codebase....

Inclusion of CAcert in Mageia dates from this Mandriva bug from 2006: https://qa.mandriva.com/show_bug.cgi?id=23171

I think Debian and downstream users of its root certificate bundle are the largest population trusting CAcert.org. That is, still, a fairly small population, and I've made some arguments in that bug report (see my post at the bottom) why it specifically doesn't make sense for a small population to carry an additional root certificate that isn't widely trusted.

Furthermore, Debian's root certificates package is explicitly documented (in the package description, see http://packages.debian.org/sid/ca-certificates for example) as just being a collection of certificates with no particular statement as to whether those certificates are trustworthy to be root certs. I couldn't find a clear policy about the intention of Mageia's rootcerts package, but note that most other distributions (including Fedora and FreeBSD) have decided that they want a useful package of default root certificates, and so they've outsourced the decision to an external entity (generally Mozilla) that runs an acceptance program involving audits. No such entity has accepted CAcert.

Reproducible: 

Steps to Reproduce:</thetext>
  </long_desc><long_desc isprivate="0">
    <commentid>90538</commentid>
    <comment_count>1</comment_count>
    <who name="David Walser">luigiwalser</who>
    <bug_when>2013-11-13 22:10:48 +0100</bug_when>
    <thetext>Maybe this will happen if we migrate to Fedora's ca-certificates package.  I know some of our users would be against this though, so probably needs some discussion on the dev mailing list.</thetext>
  </long_desc><long_desc isprivate="0">
    <commentid>90664</commentid>
    <comment_count>2</comment_count>
    <who name="Geoffrey Thomas">mageia</who>
    <bug_when>2013-11-17 01:01:59 +0100</bug_when>
    <thetext>If I understand correctly, Fedora is packaging the Mozilla bundle with no additional roots -- that definitely seems like the right policy to me. See also the discussion in https://fedorahosted.org/fesco/ticket/276 about certificate vetting. Is there an open bug I can follow about switching to the Mozilla bundle?

If you start a thread about CAcert on the dev mailing list, I'd appreciate being Cc'd. I definitely understand that this will be a change that affects existing folks relying on CAcert, but I think doing so, given CAcert's security posture, is a disservice to those users, and a huge disservice to users who aren't (intentionally) relying on CAcert.</thetext>
  </long_desc><long_desc isprivate="0">
    <commentid>132550</commentid>
    <comment_count>3</comment_count>
    <who name="Samuel Verschelde">stormi</who>
    <bug_when>2015-05-17 01:09:46 +0200</bug_when>
    <thetext>Is this bug report still valid for Mageia 4 and/or Mageia 5?</thetext>
  </long_desc><long_desc isprivate="0">
    <commentid>146238</commentid>
    <comment_count>4</comment_count>
    <who name="Marja van Waes">marja11</who>
    <bug_when>2015-11-30 14:48:07 +0100</bug_when>
    <thetext>(In reply to Geoffrey Thomas from comment #2)

&gt; If you start a thread about CAcert on the dev mailing list, I'd appreciate
&gt; being Cc'd. 

Feel free to be the one starting it, Geoffrey. You have as much right to post on that mailing list as anyone else.

I know little about certificates, but there's ca-cert in /etc/pki/tls/rootcerts/, I assume that's the same as CAcert.

[marja@localhost ~]$ ls -al /etc/pki/tls/rootcerts/ | grep ca-cert
lrwxrwxrwx 1 root root    29 nov  1 16:54 99d0fa06.0 -&gt; ca-cert-signing-authority.pem
-rw-r--r-- 1 root root  8294 nov  1 16:54 ca-cert-signing-authority.pem
[marja@localhost ~]$ 

Assigning to pkg-bugs,  because there is no maintainer.

Note that I don't know whether this bug should be fixed, or closed as wontfix.</thetext>
  </long_desc><long_desc isprivate="0">
    <commentid>147226</commentid>
    <comment_count>5</comment_count>
    <who name="Thomas Spuhler">thomas</who>
    <bug_when>2015-12-21 15:20:33 +0100</bug_when>
    <thetext>CACERT is working hard to upgrade their system with complete new software. I am not for dropping it. This is the almost the only free Cert we can get.</thetext>
  </long_desc><long_desc isprivate="0">
    <commentid>147229</commentid>
    <comment_count>6</comment_count>
    <who name="David Walser">luigiwalser</who>
    <bug_when>2015-12-21 16:21:18 +0100</bug_when>
    <thetext>(In reply to Thomas Spuhler from comment #5)
&gt; CACERT is working hard to upgrade their system with complete new software. I
&gt; am not for dropping it. This is the almost the only free Cert we can get.

With letsencrypt launching now, that's not true anymore.  Anyway, as I said before, this isn't the appropriate place to discuss this.</thetext>
  </long_desc>
      
      

    </bug>

</bugzilla>