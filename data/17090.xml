<?xml version="1.0" encoding="utf-8"?><!DOCTYPE bugzilla  SYSTEM 'https://bugs.mageia.org/page.cgi?id=bugzilla.dtd'><bugzilla maintainer="sysadmin@group.mageia.org" urlbase="https://bugs.mageia.org/" version="4.4.11">

    <bug>
          <bug_id>17090</bug_id>
          
          <creation_ts>2015-11-05 15:38:00 +0100</creation_ts>
          <short_desc>hwclock not being called on shutdown/reboot</short_desc>
          <delta_ts>2015-11-05 18:26:36 +0100</delta_ts>
          <reporter_accessible>1</reporter_accessible>
          <cclist_accessible>1</cclist_accessible>
          <classification_id>1</classification_id>
          <classification>Unclassified</classification>
          <product>Mageia</product>
          <component>RPM Packages</component>
          <version>5</version>
          <rep_platform>i586</rep_platform>
          <op_sys>Linux</op_sys>
          <bug_status>NEW</bug_status>
          <resolution/>
          
          
          <bug_file_loc/>
          <status_whiteboard/>
          <keywords/>
          <priority>Normal</priority>
          <bug_severity>normal</bug_severity>
          <target_milestone>---</target_milestone>
          
          
          <everconfirmed>1</everconfirmed>
          <reporter name="David Walser">luigiwalser</reporter>
          <assigned_to name="Colin Guthrie">mageia</assigned_to>
          
          
          <cf_rpmpkg>initscripts-9.55-13.mga5.src.rpm</cf_rpmpkg>
          <cf_cve/>

      

      

      

          <comment_sort_order>oldest_to_newest</comment_sort_order>  
          <long_desc isprivate="0">
    <commentid>144926</commentid>
    <comment_count>0</comment_count>
    <who name="David Walser">luigiwalser</who>
    <bug_when>2015-11-05 15:38:06 +0100</bug_when>
    <thetext>With the recent DST change, our system clocks were set back an hour, and unfortunately the hardware clocks are currently running in local time rather than UTC, so this means that the hardware clock needed to be set back as well.  Rebooting the machines, they come back with the old time.  We noticed this, as sssd tries to start before chrony, and kerberos fails with the clock being wrong.  Our initscripts used to call hwclock --systohc during the shutdown process, and this needs to still happen.  A different issue, but I also wonder if there's a way for sssd to wait for an ntp implementation to start if there's one enabled.

Reproducible: 

Steps to Reproduce:</thetext>
  </long_desc><long_desc isprivate="0">
    <commentid>144930</commentid>
    <comment_count>1</comment_count>
    <who name="Colin Guthrie">mageia</who>
    <bug_when>2015-11-05 16:42:58 +0100</bug_when>
    <thetext>Calling a command on shutdown does not sound like the solution, but rather a hack. I mean if power is lost, it would still break too right? I wouldn't approach this problem from a &quot;here is the solution&quot; but rather to think about it fully and then take appropriate action.

Can you tell me what the output from: &quot;timedatectl&quot; is on these machines? I'm especially interested in the &quot;RTC in local TZ&quot; result.

I'm presuming this says &quot;yes&quot; on your setup.

Can you confirm this before I think about this more? Obviously, having the hwclock in localtime is prone to problems (the man page of hwclock says this and we don't recommend it in our installer etc etc.). If it's kept in UTC, then the kernel should sync things every 11 minutes anyway... I guess if you have &quot;no&quot; in the output for &quot;RTC in local TZ&quot; that either the server was rebooted before the sync happened or, for some reason, the sync never happened and we need to investigate that more.

[Regarding sssd, you could tell sssd.service to be After=time-sync.target]</thetext>
  </long_desc><long_desc isprivate="0">
    <commentid>144936</commentid>
    <comment_count>2</comment_count>
    <who name="David Walser">luigiwalser</who>
    <bug_when>2015-11-05 18:26:36 +0100</bug_when>
    <thetext>(In reply to Colin Guthrie from comment #1)
&gt; Calling a command on shutdown does not sound like the solution, but rather a
&gt; hack. I mean if power is lost, it would still break too right? I wouldn't
&gt; approach this problem from a &quot;here is the solution&quot; but rather to think
&gt; about it fully and then take appropriate action.

Well, the time still should be synced on shutdown, but yes if you lose power you'll still have a problem.

&gt; Can you tell me what the output from: &quot;timedatectl&quot; is on these machines?
&gt; I'm especially interested in the &quot;RTC in local TZ&quot; result.

      Local time: Thu 2015-11-05 12:23:04 EST
  Universal time: Thu 2015-11-05 17:23:04 UTC
        RTC time: Thu 2015-11-05 12:23:04
       Time zone: America/New_York (EST, -0500)
     NTP enabled: yes
NTP synchronized: yes
 RTC in local TZ: yes
      DST active: no
 Last DST change: DST ended at
                  Sun 2015-11-01 01:59:59 EDT
                  Sun 2015-11-01 01:00:00 EST
 Next DST change: DST begins (the clock jumps one hour forward) at
                  Sun 2016-03-13 01:59:59 EST
                  Sun 2016-03-13 03:00:00 EDT

Warning: The system is configured to read the RTC time in the local time zone. This
         mode can not be fully supported. It will create various problems with time
         zone changes and daylight saving time adjustments. The RTC time is never updated,
         it relies on external facilities to maintain it. If at all possible, use
         RTC in UTC by calling 'timedatectl set-local-rtc 0'.

&gt; I'm presuming this says &quot;yes&quot; on your setup.

Indeed.

&gt; Can you confirm this before I think about this more? Obviously, having the
&gt; hwclock in localtime is prone to problems (the man page of hwclock says this
&gt; and we don't recommend it in our installer etc etc.). If it's kept in UTC,
&gt; then the kernel should sync things every 11 minutes anyway... I guess if you
&gt; have &quot;no&quot; in the output for &quot;RTC in local TZ&quot; that either the server was
&gt; rebooted before the sync happened or, for some reason, the sync never
&gt; happened and we need to investigate that more.

I know it's sucky having the hwclock in local time.  It's only like this because it was initially installed as a dual-boot with Windows 7 (which isn't being used anymore, so I suppose I could just change it now).  Regardless, we're supposed to be ordering new machines soon and those will be Linux-only and have their hardware clocks in UTC.  Still, hardware clocks can drift, so even if it's only off by 6 minutes, kerberos still will balk at it.

&gt; [Regarding sssd, you could tell sssd.service to be After=time-sync.target]

Thanks, I'll do that.</thetext>
  </long_desc>
      
      

    </bug>

</bugzilla>